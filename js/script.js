// Example starter JavaScript for disabling form submissions if there are invalid fields

(function () {
  "use strict";

  var forms = document.querySelectorAll(".needs-validation");

  Array.prototype.slice.call(forms).forEach(function (form) {
    form.addEventListener(
      "submit",
      function (event) {
        if (!form.checkValidity()) {
          event.preventDefault();
          event.stopPropagation();
        }

        form.classList.add("was-validated");
      },
      false
    );
  });
})();

// Slick carousel for slide Testimonial And Services

$(function () {
  var slickOpts = {
    dots: true,
    infinite: true,
    speed: 500,
    autoplay: true,
  };
});
$(document).ready(function () {
  $(".bg-advice").slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });
});

$(document).ready(function () {
  $(".testimonial-reel").slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });
});

//Search module script......................................

document.addEventListener("DOMContentLoaded", function () {
  const searchForm = document.querySelector(".search-box");
  const inputSearch = document.querySelector(".input-search");

  inputSearch.addEventListener("keydown", function (event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      const searchTerm = inputSearch.value.trim();
      if (searchTerm !== "") {
        window.location.href = `search.html?q=${encodeURIComponent(
          searchTerm
        )}`;
      }
    }
  });

  searchForm.addEventListener("submit", function (event) {
    event.preventDefault();
    const searchTerm = inputSearch.value.trim();
    if (searchTerm !== "") {
      window.location.href = `search.html?q=${encodeURIComponent(searchTerm)}`;
    }
  });
});

$(document).ready(function () {
  $("#navbarNav").on("show.bs.collapse", function () {
    $("#menuToggle").html('<span class="bi bi-x"></span>');
  });

  $("#navbarNav").on("hide.bs.collapse", function () {
    $("#menuToggle").html('<span class="navbar-toggler-icon"></span>');
  });
});

var currentUrl = window.location.href;
var menuLinks = document.querySelectorAll(".navbar-nav a");

menuLinks.forEach(function (link) {
  if (link.href === currentUrl) {
    link.parentElement.classList.add("active");
  }
});
